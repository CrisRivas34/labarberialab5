public class Client {
  private String name;
  private ClientStatus status;

  /**
   * Esta es una clase que representa un Cliente.
   * Un Cliente tiene un nombre y un estado que es un ENUM.
   */
  public Client(String name) {
    this.name = name;
    this.status = ClientStatus.WAITING;
  }

  public String getName() {
    return name;
  }

  public ClientStatus getStatus() {
    return status;
  }

  public void setStatus(ClientStatus status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "Client{" +
            "name='" + name + '\'' +
            ", status=" + status +
            '}';
  }
}
