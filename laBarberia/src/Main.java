import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Main {
  public static void main(String[] args) {
    Queue<Client> queveClients = new LinkedList<>();
    Queue<Client> queveClientsInProgress = new LinkedList<>();
    Queue<Client> listSuccesed = new LinkedList<>();

    Client client1 = new Client("c1");
    Client client2 = new Client("c2");
    Client client3 = new Client("c3");
    Client client4 = new Client("c4");
    Client client5 = new Client("c5");
    Client client6 = new Client("c6");
    Client client7 = new Client("c7");
    Client client8 = new Client("c8");

    queveClients.add(client1);
    queveClients.add(client2);
    queveClients.add(client3);
    queveClients.add(client4);
    queveClients.add(client5);
    queveClients.add(client6);
    queveClients.add(client7);
    queveClients.add(client8);

    ArrayList<Client> listClients = new ArrayList<>();
    listClients.add(client1);
    listClients.add(client2);
    listClients.add(client3);
    listClients.add(client4);
    listClients.add(client5);
    listClients.add(client6);
    listClients.add(client7);
    listClients.add(client8);

    while (listClients.get(listClients.size() - 1).getStatus() == ClientStatus.WAITING ||
            listClients.get(listClients.size() - 1).getStatus() == ClientStatus.IN_PROGRESS) {
      System.out.println("Elementos en la cola: ");

      for (int i = 0; i < listClients.size(); i++) {
        System.out.println(listClients.get(i));
      }

      while (queveClientsInProgress.isEmpty() == false) {
        for (int i = 0; i < 2; i++) {
          queveClientsInProgress.peek().setStatus(ClientStatus.SUCCESED);
          listSuccesed.add(queveClientsInProgress.poll());
        }
      }

      for (int i = 0; i < 2; i++) {
        if (queveClients.peek() != null) {
          queveClients.peek().setStatus(ClientStatus.IN_PROGRESS);
          queveClientsInProgress.add(queveClients.poll());
        }

        try {
          Thread.sleep(5000); // 5000 milisegundos = 5 segundos
        } catch (InterruptedException e) {
          // Manejar la excepción si es necesario
          e.printStackTrace();
        }

      }
    }
    for (int i = 0; i < listClients.size(); i++) {
      System.out.println(listClients.get(i));
    }
  }
}